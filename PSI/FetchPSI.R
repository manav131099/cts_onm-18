rm(list = ls())
errHandle = file('/home/admin/Logs/LogsPSIFetch.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
require('httr')
dircheck = function(x)
{
  if(!file.exists(x))
    dir.create(x)  
}

#Update this array to add stations
#Get tz value using the OlsonNames() function

stations = c('Delhi','Beijing','Shanghai','Los Angeles','Mexico City','Singapore',
'London','Moscow','Dhaka','Tehran','Lima','Bogota','Richards Bay')

urlnames = c('india/new-delhi/us-embassy','beijing','shanghai','losangeles/los-angeles-north-main-street',
'mexico/mexico/merced','singapore/central','London','moscow/glebovskaya','bangladesh/dhaka/us-consulate',
'iran/tehran/darous','Lima','colombia/bogota/us-consulate','south-africa/richardsbay/brackenham')

tz = c('Asia/Calcutta','Asia/Shanghai','Asia/Shanghai','America/Los_Angeles','America/Mexico_City',
'Singapore','Europe/London','Europe/Moscow','Asia/Dhaka',
'Iran','America/Lima','America/Bogota','Africa/Johannesburg')

path = '/home/admin/Dropbox/PSI-Data'
dircheck(path)
while(1)
{
for(x in  1 : length(stations))
{
  print(paste('Getting',stations[x]))
	print(paste('Using url',urlnames[x]))
	print(paste('using timezone',tz[x]))
  pathstation = paste(path,stations[x],sep="/")
  dircheck(pathstation)
  timelocal = format(Sys.time(),tz=tz[x],usetz=T)
  timelocal = as.character(timelocal)
  yr = substr(timelocal,1,4)
  mon = substr(timelocal,1,7)
  date = substr(timelocal,1,10)
  index = as.numeric(substr(timelocal,12,13))+1
  indextrue = index-1
  if(indextrue<10)
  {
    indextrue=paste("0",indextrue,sep="")
  }
  pathyr = paste(pathstation,yr,sep="/")
  dircheck(pathyr)
  pathmon=paste(pathyr,mon,sep="/")
  dircheck(pathmon)
  pathday = paste(pathmon,"/",stations[x]," PSI ",date,".txt",sep="")
  fetchdata = try(httr::POST(paste('http://api.waqi.info/feed/',urlnames[x],'/?token=4124c96f4231b8fdc0d79ea3af349d0c32c05818',sep="")),silent=T)
  val = NA
  if(class(fetchdata)!='try-error')
  {
    print('Got data successfully')
		fetchdata2 = content(fetchdata)
    pm25 = try(fetchdata2$data$iaqi$pm25,silent=F)
		if(stations[x]=='Richards Bay')
		{
			pm25 = try(fetchdata2$data$iaqi$pm10,silent=F)
		}
    if(!is.null(pm25))
    {
      val=as.numeric(unlist(pm25))
    }
  }
  {
    if(!file.exists(pathday))
    {
    print('Creating new file')
      timedf = c()
      valdf = c()
      timedf[index] = paste(indextrue,":00",sep="")
      valdf[index] = val
      df = data.frame(Tm=timedf,Pm25=valdf,stringsAsFactors=F)
    }
    else
    {
    print('File exists so will change value if finite')
      dataread = read.table(pathday,header = T,sep="\t",stringsAsFactors=F)
      if(is.finite(val))
      {
    print('Updating file as value is finite')
		    if(!is.finite(as.numeric(indextrue)))
				{
					print(paste('Indextrue isnt finite and is ',indextrue))
					print(paste('Index value is ',index))
					indextrue=index-1
				}
				time = as.character(dataread[,1])
				value = as.numeric(dataread[,2])
        time[index] = paste(indextrue,":00",sep="")
        value[index] = val
				dataread = data.frame(Tm=time,Pm25=value,stringsAsFactors=F)
      }
      df = dataread
    }
  }
  write.table(df,file=pathday,row.names = F,col.names = T,sep="\t",append = F)
  print(paste('Done with',stations[x]))
}
Sys.sleep(30*60)
}
