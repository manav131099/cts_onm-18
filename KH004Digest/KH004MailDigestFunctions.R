rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
TIMESTAMPSALARM4 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
INSTCAP=c(181.44,362.88)
FILEPATHCOMP = ""
Eac1Recorded = 0
Eac2Recorded = 0
GModRecorded = 0
GPyRecorded = 0
Yld1Recorded = 0
Yld2Recorded = 0

resetGlobals = function()
{
Eac1Recorded <<- 0
Eac2Recorded <<- 0
GModRecorded <<- 0
GPyRecorded <<- 0
Yld1Recorded <<- 0
Yld2Recorded <<- 0
}

timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
if(T){
getPRData = function(df,no)
{
	PR1=PR2=gsi=gpy=PR3=PR4=NA
	date = as.character(df[,1])
	yr = substr(date,1,4)
	mon=substr(date,1,7)
	date = substr(date,1,10)
	path = paste('/home/admin/Dropbox/Second Gen/[KH-003S]/',yr,"/",mon,"/[KH-003S] ",date,".txt",sep="")
	path2 = paste('/home/admin/Dropbox/Second Gen/[KH-714S]/',yr,"/",mon,"/[KH-714S] ",date,".txt",sep="")
	print(paste('Path reading GSI is from',path))
	if(file.exists(path))
	{
	print('File exists')
	dataread = read.table(path,header = T,sep = "\t")
	{
	if(no==2 || no==4){
	dsp1 = as.numeric(df[1,9])
	dsp2 = as.numeric(df[1,10])
	}
	else if(no==3 || no==1)
		dsp1 = as.numeric(df[1,5])
	}
	gsi = as.numeric(dataread[1,4])
	GModRecorded <<- gsi
	PR1 = format(round((dsp1 *100/ gsi),1),nsmall=1)
	if(no ==2 || no == 4)
		PR2 = format(round((dsp2 *100/ gsi),1),nsmall=1)
	}
	if(file.exists(path2))
	{
	print('File exists')
	dataread = read.table(path2,header = T,sep = "\t")
	{
	if(no==2 || no==4){
	dsp1 = as.numeric(df[1,9])
	dsp2 = as.numeric(df[1,10])
	}
	else if(no==3 || no==1)
		dsp1 = as.numeric(df[1,5])
	}
	gpy = as.numeric(dataread[1,6])
	GPyRecorded <<- gpy
	PR3 = format(round((dsp1 *100/ gpy),1),nsmall=1)
	if(no ==2 || no == 4)
		PR4 = format(round((dsp2 *100/ gpy),1),nsmall=1)
	}
	array2 = c(PR1,gsi,PR3,gpy)
	if(no == 2 || no ==4)
		array2 = c(PR1,PR2,gsi,PR3,PR4,gpy)
	return(array2)
}
}

getArtificialLoad = function(filepath)
{
	print('Artificial load')
	print('--------------------------------')
	print(filepath)
	print(FILEPATHCOMP)
	print('--------------------------------')
	dataread = try(read.table(filepath,header=T,sep = "\t",stringsAsFactors=F),silent = T)
	dataread2 = try(read.table(FILEPATHCOMP,header=T,sep="\t",stringsAsFactors=F),silent = T)
	if(class(dataread)=='try-error' || class(dataread2)=='try-error' || ncol(dataread2) < 20)
	{
		return (NA)
	}
	artificialLoad = unlist(rep(NA,length(dataread[,1])))
	c1 = as.numeric(dataread[,15])
	c2 = as.numeric(dataread2[,20])
	idxmtch = match(as.character(dataread2[,1]),as.character(dataread[,1]))
	idxmtch2 = match(as.character(dataread[,1]),as.character(dataread2[,1]))
	idxmtch = idxmtch[complete.cases(idxmtch)]
	idxmtch2 = idxmtch2[complete.cases(idxmtch2)]
	c1 = c1[idxmtch]
	c2 = c2[idxmtch2]
	artificialLoad[idxmtch] = c1 + c2
	cnms = colnames(dataread)
	dataread[,(ncol(dataread)+1)] = as.character(artificialLoad)
	colnames(dataread) = c(cnms,"ArtificialLoad")
	print('Updating Gen-1 data')
	write.table(dataread,file=filepath,row.names=F,col.names=T,sep="\t",append=F)
	return(round(sum(artificialLoad[complete.cases(artificialLoad)])/60000,2))
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL
			TIMESTAMPSALARM2 <<- NULL
			TIMESTAMPALARM3 <<- NULL
			TIMESTAMPALARM4 <<- NULL}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3)
		{
			TIMESTAMPALARM3 <<- NULL
		}
		else if(METERCALLED ==4)
		{
			TIMESTAMPALARM4 <<- NULL
		}
	}
	print(paste('IN 2G',filepath))
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	if(class(dataread)=='try-error' || ncol(dataread) < 15)
	{
		{
		if(METERCALLED == 1 || METERCALLED == 3)
		{
      df = data.frame(Date = NA, LoadEac = NA,
                  DA = NA,Downtime = NA,Yld=NA, PR = NA, GSITot = NA,PRPy=NA,PyTot=NA,
									ExpTot = NA,stringsAsFactors = F)
		}
		else if (METERCALLED == 2 || METERCALLED == 4)
		{
      df = data.frame(Date = NA, SolarPowerMeth1 = NA,SolarPowerMeth2= NA,
                  Ratio=NA,DA = NA,Downtime = NA,
									LastTime = NA, LastRead = NA, Yld1=NA,Yld2=NA,
									PR1 =NA, PR2 =NA,GSITot=NA,ArtificialLoad=NA,PRPy1=NA,PRPy2=NA,PyTot=NA,stringsAsFactors = F)
		}
		}
		write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  	return(df)
	}
	dataread2 = dataread
	if(METERCALLED == 2 || METERCALLED == 4)
	{
	dataread = dataread2[complete.cases(as.numeric(dataread2[,39])),]
	lasttime=lastread=Eac2=NA
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),39])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),39]) - as.numeric(dataread[1,39]))/1000,2),nsmall=1)
	if(METERCALLED == 2)
		Eac1Recorded <<- as.numeric(Eac2)
	if(METERCALLED == 4)
		Eac2Recorded <<- as.numeric(Eac2)
	}
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,15])),]
	dataread = dataread[as.numeric(dataread[,15]) > 0,]
	Eac1 = RATIO=NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,15]))/60000,1),nsmall=2)
		if(METERCALLED == 2 || METERCALLED == 4)
			RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	}
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,15])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,15]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 3){
		 	 TIMESTAMPSALARM3 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 4){
		 	 TIMESTAMPSALARM4 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
#	print(paste('Date',date))
#	print(paste('EAC1',Eac1))
#	print(paste('Eac2',Eac2))
#	print(paste('RATIO',RATIO))
#	print(paste('DA',DA))
#	print(paste('Downtime',totrowsmissing))
#	print(paste('Lasttime',lasttime))
#	print(paste('LastRead',lastread))
		indextouse = ceiling(METERCALLED/2)
	{
	  if(METERCALLED == 1 || METERCALLED == 3)
	  {
			Yld = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
			ExpTot = dataread[complete.cases(as.numeric(dataread[,13])),]
			{
			if(ncol(ExpTot))
			{
				val = as.numeric(ExpTot[,13])
				val = val[val < 0]
				if(length(val))
   			ExpTot = format(round(sum(val)/-60000,2),nsmall=2)
				else
				ExpTot = NA
      }
			else
				ExpTot = NA
			}
			df = data.frame(Date = date, LoadEac = as.numeric(Eac1),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld,stringsAsFactors = F)
			vals = getPRData(df,METERCALLED)
      df = data.frame(Date = date, LoadEac = as.numeric(Eac1),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld, PR = vals[1], GSITot = vals[2],
									PRPy=vals[3],PyTot=vals[4],ExpTot=ExpTot,stringsAsFactors = F)
    } 
	  else if(METERCALLED == 2 || METERCALLED == 4)
	  {
			Yld1 = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
			Yld2 = format(round(as.numeric(Eac2)/INSTCAP[indextouse],2),nsmall=2)
			if(METERCALLED == 2)
				Yld1Recorded <<- as.numeric(Yld2)
			if(METERCALLED == 4)
				Yld2Recorded <<- as.numeric(Yld2)
			df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, Yld1=Yld1,Yld2=Yld2,stringsAsFactors = F)
			vals = getPRData(df,METERCALLED)
      artificial = getArtificialLoad(filepath)
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, Yld1=Yld1,Yld2=Yld2,
									PR1 = vals[1], PR2 = vals[2],GSITot=vals[3],ArtificialLoad=artificial,
									PRPy1 = vals[4],PRPy2 = vals[5],PyTot=vals[6],stringsAsFactors = F)
		}
#	  else if(METERCALLED == 3)
#	  {
#			#DailyCons = dataread[,ncol(dataread)]
#			#DailyCons = DailyCons[complete.cases(DailyCons)]
#			#DailyCons = round(sum(DailyCons)/60000,2)
#      df = data.frame(Date = date, SolaPowerMeth1 = as.numeric(Eac1),CokePowerMeth2= as.numeric(Eac2),
#                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
#									LastTime = lasttime, LastRead = lastread, 
#									#DailyCons = DailyCons,
#									stringsAsFactors = F)
#	  }
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,filepathm4,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread4 = read.table(filepathm4,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	artificial1 = as.numeric(dataread2[,13])
	artificial2 = as.numeric(dataread4[,13])

	dt = as.character(dataread2[,6])
	da = as.character(dataread2[,5])
	EacLoad1 = as.numeric(dataread1[,2])
	EacLoad2 = as.numeric(dataread3[,2])
	EacSol1Meth1 = as.numeric(dataread2[,2])
	EacSol1Meth2 = as.numeric(dataread2[,3])
	RatioSol1 = as.numeric(dataread2[,4])
	EacSol2Meth1 = as.numeric(dataread4[,2])
	EacSol2Meth2 = as.numeric(dataread4[,3])
	RatioSol2 = as.numeric(dataread4[,4])
	LastReadSol1 = as.numeric(dataread2[,8])
	LastReadSol2 = as.numeric(dataread4[,8])
	LastTimeSol1 = as.numeric(dataread2[,7])
	LastTimeSol2 = as.numeric(dataread4[,7])
	GsiTot = as.numeric(dataread1[,7])
	GPyTot = as.numeric(dataread1[,9])
	PR11 = as.numeric(dataread1[,6])
	PR31 = as.numeric(dataread3[,6])
	PR1Py = as.numeric(dataread1[,8])
	PR3Py = as.numeric(dataread3[,8])
	PR21 = as.numeric(dataread2[,11])
	PR22 = as.numeric(dataread2[,12])
	PR41 = as.numeric(dataread4[,11])
	PR42 = as.numeric(dataread4[,12])
	PR21Py = as.numeric(dataread2[,14])
	PR22Py = as.numeric(dataread2[,15])
	PR41Py = as.numeric(dataread4[,14])
	PR42Py = as.numeric(dataread4[,15])
	ExpTotM1 = as.numeric(dataread1[,10])
	ExpTotM3 = as.numeric(dataread3[,10])
	ExpPerc1 = round(100*ExpTotM1/EacSol1Meth2,1)
	ExpPerc3 = round(100*ExpTotM3/EacSol2Meth2,1)
#	CokeLoadMeth1=ArtificialLoad=CokeLoadMeth2=PercentageArtSolar=RatioCokeLoad=NA
#	ltCokeLoad=lrCokeLoad=DailyCons=NA
# if(!(is.null(filepathm3)))
#	{
#	CokeLoadMeth1 = as.numeric(dataread3[,2])
#	CokeLoadMeth2 = as.numeric(dataread3[,3])
#	#ArtificialLoad = CokeLoadMeth1 + EacCokeMeth1
#	#PercentageArtSolar = round(((EacCokeMeth1*100) / (ArtificialLoad)),2)
#	}
#	SolarEnergyMeth1 = as.numeric(dataread1[,2])
#	SolarEnergyMeth2 = as.numeric(dataread1[,3])
#  RatioCoke = as.numeric(dataread2[,4])
#  RatioSolar = as.numeric(dataread1[,4])
#  if(!(is.null(filepathm3)))
#		RatioCokeLoad = as.numeric(dataread3[,4])
#	CABLOSS= round((EacCokeMeth2/SolarEnergyMeth2),3)
#	CABLOSS = abs(1 - CABLOSS)*100
#	CABLOSSTOPRINT <<- format(CABLOSS,nsmall=2)
#	ltSol = as.character(dataread1[,7])
#	lrSol = as.character(dataread1[,8])
#	ltCoke = as.character(dataread2[,7])
#	lrCoke = as.character(dataread2[,8])
#  if(!(is.null(filepathm3)))
#	{
#	ltCokeLoad = as.character(dataread3[,7])
#	lrCokeLoad = as.character(dataread3[,8])
#	#DailyCons = as.character(dataread3[,9])
#  }
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	EacLoad1 = EacLoad1,
	EacLoad2 = EacLoad2,
	EacSol1Meth1=EacSol1Meth1,
	EacSol1Meth2=EacSol1Meth2,
	RatioSol1=RatioSol1,
	LastTimeSol1 = LastTimeSol1,
	LastReadSol1 = LastReadSol1,
	EacSol2Meth1=EacSol2Meth1,
	EacSol2Meth2=EacSol2Meth2,
	RatioSol2=RatioSol2,
	LastTimeSol2 = LastTimeSol2,
	LastReadSol2 = LastReadSol2,
	GsiTot = GsiTot,
	PRM1 = PR11,
	PRM2Meth1 = PR21,
	PRM2Meth2 = PR22,
	PRM3 = PR31,
	PRM4Meth1 = PR41,
	PRM4Meth2 = PR42,
	ArtificialM1 = artificial1,
	ArtificialM2 = artificial2,
	GPyTot = GPyTot,
	PR1Py=PR1Py,
	PR21Py=PR21Py,
	PR22Py = PR22Py,
	PR41Py=PR41Py,
	PR42Py=PR42Py,
	ExpTotM1=ExpTotM1,
	ExpTotM3=ExpTotM3,
	ExpPercM1=ExpPerc1,
	ExpPercM3=ExpPerc3,
	stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
		data = read.table(writefilepath,header=T,sep="\t",stringsAsFactors=F)
		dates = as.character(data[,1])
		idxmtch = match(substr(as.character(dataread1[1,1]),1,10),dates)
		if(is.finite(idxmtch))
		{
			data = data[-idxmtch,]
			write.table(data,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
		}
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

