system('rm -R "/home/admin/Dropbox/Second Gen/[KH-006X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[KH-006X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/KH006XDigest/KH006MailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[KH-006X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[KH-006X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[KH-006X]'
checkdir(writepath3G)
DAYSALIVE = 0
years = dir(path)

stnnickName2 = "KH-006X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"-M1] ",lastdatemail,".txt",sep="")
ENDCALL=0

for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[KH-006X] ",months[y],".txt",sep="")
    stations = dir(pathmonths)
		pathdays = paste(pathmonths,stations[1],sep="/")
    pathdays2 = paste(pathmonths,stations[2],sep="/")
    pathdays3 = paste(pathmonths,stations[3],sep="/")
    pathdays4 = paste(pathmonths,stations[4],sep="/")
		writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
    writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
    writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
    writepath2Gdays4 = paste(writepath2Gmon,stations[4],sep="/")
    checkdir(writepath2Gdays)
    checkdir(writepath2Gdays2)
    checkdir(writepath2Gdays3)
    checkdir(writepath2Gdays4)
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    days2 = dir(pathdays2)
		days3 = dir(pathdays3)
		days4 = dir(pathdays4)
		if(length(days) != length(days2))
		{
			minlen = min(length(days),length(days2),length(days3),length(days4))
			days = tail(days,n=minlen)
			days2 = tail(days2,n=minlen)
			days3 = tail(days3,n=minlen)
			days4 = tail(days4,n=minlen)
		}
		if(length(stations)>4)
		{
    	pathdays5 = paste(pathmonths,stations[5],sep="/")
    	writepath2Gdays5 = paste(writepath2Gmon,stations[5],sep="/")
    	checkdir(writepath2Gdays5)
			days5 = dir(pathdays5)
			tstart = length(days) - length(days5) 
		}
      for(t in 1 : length(days))
      {
        if(ENDCALL==1)
					break
				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
        writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
        writepath2Gfinal4 = paste(writepath2Gdays4,"/",days4[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
        readpath2 = paste(pathdays2,days2[t],sep="/")
        readpath3 = paste(pathdays3,days3[t],sep="/")
        readpath4 = paste(pathdays4,days4[t],sep="/")
				resetGlobals()
				METERCALLED <<- 1 #correct dont change
        df1 = secondGenData(readpath,writepath2Gfinal)
				METERCALLED <<- 3 # correct dont change
        df2 = secondGenData(readpath2,writepath2Gfinal2)
				METERCALLED <<- 2 # correct dont change
        df3 = secondGenData(readpath3,writepath2Gfinal3)
				METERCALLED <<- 4 # correct dont change
        df4 = secondGenData(readpath4,writepath2Gfinal4)
				print(days[t])
				print(days2[t])
				print(days3[t])
				print(days4[t])
				writepath2Gfinal5 = NA
				if(length(stations)>4 && t > tstart)
				{
        	writepath2Gfinal5 = paste(writepath2Gdays5,"/",days5[t],sep="")
        	readpath5 = paste(pathdays5,days5[(t-tstart)],sep="/")
					METERCALLED <<- 5 # correct dont change
        	df5 = secondGenData(readpath5,writepath2Gfinal5)
				}
        thirdGenData(writepath2Gfinal,writepath2Gfinal3,
				writepath2Gfinal2,writepath2Gfinal4,writepath2Gfinal5,writepath3Gfinal) #Correct don't change order
				print(writepath3Gfinal)
				if(days[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL == 1)
				break
    }
		if(ENDCALL == 1)
			break
}
print('Historical Analysis Done')
