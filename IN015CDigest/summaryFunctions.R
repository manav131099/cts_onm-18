require('compiler')
checkdirUnOp = function(path)
{
  if(!file.exists(path))
  {
    dir.create(path)
  }
}
checkdir = cmpfun(checkdirUnOp)
INSTCAP = 1814.4
INSTCAPM = c(1814.4, 1814.4)
NOMETERS = 2
NOINVS = 24
getGTIDataUnOp = function(date)
{
  yr = substr(date, 1, 4)
  yrmon = substr(date, 1, 7)
  filename = paste("[712] ", date, ".txt", sep = "")
  filename_2G = paste("[IN-015S] ", date, ".txt", sep = "")
  path = "/home/admin/Dropbox/Second Gen/[IN-015S]"
  path2 = "/home/admin/Dropbox/Cleantechsolar/1min/[712]"
  pathRead = paste(path, yr, yrmon, filename_2G, sep = "/")#2G Data
  pathRead2 = paste(path2, yr, yrmon, filename, sep = "/")#1G Data
  GTIGreater20 = NA
  GTI = DNI = NA
  print(pathRead)
  print('^^ In getGTIUnOp')
  if (file.exists(pathRead))
  {
    print('entered')
    dataread = read.table(pathRead, sep = "\t", header = T)
    if (nrow(dataread) > 0)
    {
      GTI = as.numeric(dataread[1, 3])
      print(GTI)
      DNI = as.numeric(dataread[1, 3])
    }
  }
}
getGTIData = cmpfun(getGTIDataUnOp)
timetominsUnOp = function(time)
{
  hr = as.numeric(substr(time,12,13))
  min = as.numeric(substr(time,15,16))
  bucket =((hr*60) + min + 1)
  return(bucket)
}
timetomins = cmpfun(timetominsUnOp)
secondGenDataUnOp = function(pathread, pathwrite,type)
{
  dataread = try(read.table(pathread,sep="\t",header = T,stringsAsFactors=F),silent = T)
  if(class(dataread) == "try-error")
  {
    print(paste('pathread error',pathread))
    date=DA=Eac1=Eac3=Eac2=Yld1=Yld2=LR=Tamb=Tmod=GTI=DNI=LT=TModSH=TambSH=PR1D=PR2D=PR1=PR2=GA=PA=IA=NA
    {
      if(type == 0)
        data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1, Yld2 = Yld2, PR1=PR1,PR2=PR2, LastRead = LR, LastTime = LT,GA=GA,PA=PA,stringsAsFactors=F)
      else if(type == 1)
        data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3, Yld1 = Yld1, Yld2 = Yld2,LastRead = LR, LastTime = LT,IA=IA,stringsAsFactors=F)
      else if(type == 2)
        data = data.frame(Date = date,DA = DA,GTI = GTI, Tamb = Tamb, Tmod = Tmod,TambSH=TambSH,TModSH=TModSH,DNI=DNI,stringsAsFactors=F)
    }
    write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
    thirdGenData(pathwrite)
    fourthGenData(pathwrite)
    return()
  }
  date = substr(dataread[1,1],1,10)
  DA = format(round((nrow(dataread)/2.88),1),nsmall=1)
  {
    if(type == 0)
    {
      idxPower = 32
      idxEnergy = 65
      Eac1 = Eac2 = Yld1 = Yld2 = LR = LT =PR1 = PR2 = PR1D = PR2D = GA = PA = NA
      idxmtchdt = 1
      {
        if(grepl("LT_MFM",pathread))
          idxmtchdt = 2
      }
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
      dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
      dataIntTr = round(dataInt/1000000,0)
      dataIntTr2 = unique(dataIntTr)
      mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
      dataInt = dataInt[abs(dataIntTr - mode) <= 100]
      dataInt2 = dataInt2[abs(dataIntTr - mode) <= 100]
      if(length(dataInt))
      {
        Eac2 = round((abs(dataInt[length(dataInt)] - dataInt[1])),1)
        LR = dataInt[length(dataInt)]
        LT = dataInt2[length(dataInt2)]
      }
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPower])),idxPower])
      dataInt = dataInt[abs(dataInt) < 1000000]
      if(length(dataInt))
        Eac1 = format(round((sum(dataInt)/12),1),nsmall=1)
      Yld1 = round(as.numeric(Eac1)/INSTCAPM[idxmtchdt],2)
      Yld2 = round(as.numeric(Eac2)/INSTCAPM[idxmtchdt],2)
      GTI = getGTIData(date)
      GTIGreater20=GTI[-c(1, 2)]
      if(length(GTIGreater20))
      {
        dataread1=dataread[complete.cases(dataread[11]),]
        VoltageGreater=dataread1[dataread1[,11]>150,1]#Found timestamps having Avg Voltage>150 here
        dataread2=dataread[complete.cases(dataread[32]),]
        PowerGreater=dataread2[dataread2[,32]>2,1]#Found timestamps having Power>2 here
        common=intersect(GTIGreater20,VoltageGreater)
        common2=intersect(GTIGreater20,PowerGreater)
        GA=(length(common)/length(GTIGreater20))*100
        GA=round(GA,1)
        PA=(length(common2)/length(GTIGreater20))*100
        PA=round(PA,1)
      }
      DNI = as.numeric(GTI[2])
      GTI = as.numeric(GTI[1])
      PR1D = round(Yld1*100/DNI,1)
      PR2D = round(Yld2*100/DNI,1)
      PR1 = round(Yld1*100/GTI,1)
      PR2 = round(Yld2*100/GTI,1)
      print(paste('Date',date))
      print(paste('DA',DA))
      print(paste('Eac1',Eac1))
      print(paste('Eac2',Eac2))
      print(paste('Yld1',Yld1))
      print(paste('Yld2',Yld2))
      print(paste('PR1',PR1))
      print(paste('PR2',PR2))
      print(paste('LastRead',LR))
      print(paste('LT',LT))
      print(paste('PR1D',PR1D))
      print(paste('PR2D',PR2D))
      print(paste('GA',GA))
      print(paste('PA',PA))
      data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1, 
                        Yld2 = Yld2, PR1=PR1, PR2 = PR2, LastRead = LR,LastTime = LT,PR1D=PR1D,PR2D=PR2D,GA=GA,PA=PA,stringsAsFactors = F)
    }
    else if(type == 1)
    {
      InvNo = unlist(strsplit(pathread,"_Inverter_"))
      InvNo = unlist(strsplit(InvNo[2],"/"))
      InvNo = as.numeric(InvNo[1])
      INSTCAPMT = 75.6
      idxEnergy = 33
      idxPowerAC = 15
      idxPowerDC = 21
      Eac1 = Eac2 = Eac3 = LR = LT=IA=NA
      GTI = getGTIData(date)
      GTIGreater20=GTI[-c(1, 2)]
      if(length(GTIGreater20))
      {
        dataread=dataread[complete.cases(dataread[21]),]
        DCPowerGreater2=dataread[dataread[,21]>0,1]#Found timestamps having DCPower>0 here
        common=intersect(GTIGreater20,DCPowerGreater2)
        IA=(length(common)/length(GTIGreater20))*100
        IA=round(IA,1)
      }
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
      dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
      dataIntTr = round(dataInt/1000000,0)
      dataIntTr2 = unique(dataIntTr)
      mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
      dataInt = dataInt[abs(dataIntTr - mode) <= 1]
      dataInt2 = dataInt2[abs(dataIntTr - mode) <= 1]
      if(length(dataInt))
      {
        Eac2 = round((abs(dataInt[length(dataInt)] - dataInt[1])/1000),1)
        LR = dataInt[length(dataInt)]/1000
        LT = dataInt2[length(dataInt2)]
      }
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerAC])),idxPowerAC])
      dataInt = dataInt[abs(dataInt) < 1000000]
      if(length(dataInt))
        Eac1 = format(round((sum(dataInt)/12000),1),nsmall=1)
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerDC])),idxPowerDC])
      dataInt = dataInt[abs(dataInt) < 1000000]
      if(length(dataInt))
        Eac3 = format(round((sum(dataInt)/12000),1),nsmall=1)
      Yld1 = round(as.numeric(Eac1)/INSTCAPMT,2)
      Yld2 = round(as.numeric(Eac2)/INSTCAPMT,2)
      data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
                        Yld1 = Yld1, Yld2 = Yld2, LastRead = LR, LastTime = LT,IA=IA,stringsAsFactors = F)
    }
    else if(type == 2)
    {
      idxGTI = 3
      idxTMod = 8
      idxTamb = 9
      idxDNI = 3
      
      Tamb = Tmod = GTI = DNI=TModSH=TambSH=NA
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTMod])),idxTMod])
      if(length(dataInt))
        Tmod = round(mean(dataInt),1)
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTamb])),idxTamb])
      if(length(dataInt))
        Tamb = round(mean(dataInt),1)
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxGTI])),idxGTI])
      if(length(dataInt))
        GTI = format(round((sum(dataInt)/12000),2),nsmall=2)
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxDNI])),idxDNI])
      if(length(dataInt))
        DNI = format(round((sum(dataInt)/12000),2),nsmall=2)
      dataInt = dataread[complete.cases(as.numeric(dataread[,idxTamb])),c(1,idxTamb)]
      if(length(dataInt[,1]))
      {
        tmmins = timetomins(as.character(dataInt[,1]))
        dataInt = dataInt[tmmins < 1140,]
        tmmins = tmmins[tmmins < 1140]
        if(length(tmmins))
        {
          dataInt = dataInt[tmmins > 420,2]
          if(length(dataInt))
            TambSH= format(round(mean(as.numeric(dataInt)),1),nsmall=1)
        }
      }
      dataInt = dataread[complete.cases(as.numeric(dataread[,idxTMod])),c(1,idxTMod)]
      if(length(dataInt[,1]))
      {
        tmmins = timetomins(as.character(dataInt[,1]))
        dataInt = dataInt[tmmins < 1140,]
        tmmins = tmmins[tmmins < 1140]
        if(length(tmmins))
        {
          dataInt = dataInt[tmmins > 420,2]
          if(length(dataInt))
            TModSH = format(round(mean(as.numeric(dataInt)),1),nsmall=1)
        }
      }
      data = data.frame(Date = date,DA = DA,GTI = GTI, Tamb = Tamb, Tmod = Tmod,
                        TambSH = TambSH,TModSH=TModSH,DNI=DNI,stringsAsFactors=F)
    }
  }
  write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
  print("done writing")
  thirdGenData(pathwrite)
  fourthGenData(pathwrite)
}
secondGenData = cmpfun(secondGenDataUnOp)
thirdGenDataUnOp = function(path2G)
{
  stnnames = unlist(strsplit(path2G,"/"))
  path3G = paste("/home/admin/Dropbox/FlexiMC_Data/Third_Gen",stnnames[7],sep="/")
  checkdir(path3G)
  path3GStn = paste(path3G,stnnames[10],sep="/")
  checkdir(path3GStn)
  pathwrite3GFinal = paste(path3GStn,paste(substr(stnnames[9],1,18),".txt",sep=""),sep="/")
  dataread = read.table(path2G,header = T,sep ="\t",stringsAsFactors=F)
  {
    if(!file.exists(pathwrite3GFinal))
      write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
    else
      write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=F,append=T,sep="\t")
  }
  dataread = read.table(pathwrite3GFinal,header = T,sep="\t",stringsAsFactors=F)
  tm = as.character(dataread[,1])
  tmuniq = unique(tm)
  # The idea here is update third-gen with the most recent readings. So if
  # there are multiple calls, only take the latest row for the day and
  # slot that into the third-gen data
  if(length(tm) > length(tmuniq))
  {
    dataread = dataread[-(nrow(dataread)-1),]
  }
  write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
}
thirdGenData = cmpfun(thirdGenDataUnOp)
fourthGenDataUnOp = function(path2G)
{
  order = c("HT_MFM", "LT_MFM", "S_Inverter_1", "S_Inverter_2", "S_Inverter_3", "S_Inverter_4", "S_Inverter_5", "S_Inverter_6", "S_Inverter_7", "S_Inverter_8", "S_Inverter_9", "S_Inverter_10", "S_Inverter_11", "S_Inverter_12", "SN_Inverter_13", "N_Inverter_14","N_Inverter_15","N_Inverter_16","N_Inverter_17","N_Inverter_18","N_Inverter_19","N_Inverter_20","N_Inverter_21","N_Inverter_22","N_Inverter_23","N_Inverter_24")
  CONSTANTLEN = c(7,unlist(rep(13,NOMETERS)),unlist(rep(9,NOINVS))) # No of columns for each file excluding date
  stnnames = unlist(strsplit(path2G,"/"))
  rowtemplate = unlist(rep(NA,sum(CONSTANTLEN)+1))
  path4G = paste("/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen",stnnames[7],sep="/")
  checkdir(path4G)
  dataread = read.table(path2G,header = T,sep ="\t",stringsAsFactors=F)
  pathfinal = paste(path4G,paste(stnnames[7],"-lifetime.txt",sep=""),sep="/")
  idxmtch = match(stnnames[10],order)
  start = 2
  if(idxmtch > 1)
  {
    start = 2 + sum(CONSTANTLEN[1:(idxmtch -1)])
  }
  end = start + CONSTANTLEN[idxmtch] - 1
  idxuse = start : end
  colnames2G = colnames(dataread)
  colnames2G = colnames2G[-1]
  colnames2G = paste(stnnames[10],colnames2G,sep="-")
  {
    if(!file.exists(pathfinal))
    {
      colnames = rep("Date",length(rowtemplate))
      colnames[idxuse] = colnames2G
      rowtemplate[idxuse] = dataread[1,(2:ncol(dataread))]
      rowtemplate[1] = as.character(dataread[1,1])
      dataExist = data.frame(rowtemplate,stringsAsFactors=F)
      colnames(dataExist) = colnames
      write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
    }
    else
    {
      dataExist = read.table(pathfinal,sep="\t",header=T,stringsAsFactors=F)
      colnames = colnames(dataExist)
      colnames[idxuse] = colnames2G
      dates = as.character(dataExist[,1])
      idxmtchdt = match(as.character(dataread[,1]),dates)
      if(is.finite(idxmtchdt))
      {
        tmp = dataExist[idxmtchdt,]
        tmp[idxuse] = dataread[1,2:ncol(dataread)]
        dataExist[idxmtchdt,] = tmp
        colnames(dataExist) = colnames
        write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
      }
      else
      {
        rowtemplate = unlist(rowtemplate)
        rowtemplate[c(1,idxuse)] = dataread[1,]
        dataExist = data.frame(rowtemplate,stringsAsFactors=F)
        colnames(dataExist) = colnames
        write.table(dataExist,file=pathfinal,row.names =F,col.names=F,sep="\t",append=T)
      }
    }
  }
  print(paste("4G Done",path2G))
}
fourthGenData = cmpfun(fourthGenDataUnOp)