rm(list = ls())
library(stringr)
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN015CHistorical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN015CDigest/summaryFunctions.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(1,2,14,18,19,20,21,22,23,24,25,15,16,17,26,3,4,5,6,7,8,9,10,11,12,13)

if(RESETHISTORICAL)
{
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-015C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-015C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-015C]')
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-015C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-015C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-015C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-015C]"


checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 1 : length(years))
{
  if(as.numeric(years[x])<2020)
    next
  pathyr = paste(path,years[x],sep="/")
  pathwriteyr = paste(pathwrite2G,years[x],sep="/")
  checkdir(pathwriteyr)
  months = dir(pathyr)
  if(!length(months))
    next
  for(y in 1 : length(months))
  {
    if(years[x] == '2020')
      if(as.numeric(substr(months[y],6,7))<6)
        next
    pathmon = paste(pathyr,months[y],sep="/")
    pathwritemon = paste(pathwriteyr,months[y],sep="/")
    checkdir(pathwritemon)
    stns = dir(pathmon)
    if(!length(stns))
      next
    stns = stns[reorderStnPaths]
    
    for(z in 1 : length(stns))
    {
        type = 2
        pathstn = paste(pathmon,stns[z],sep="/")
        if(grepl("MFM",stns[z]))
          type = 0
        if(grepl("Inverter",stns[z]))
          type = 1
      pathwritestn = paste(pathwritemon,stns[z],sep="/")
      checkdir(pathwritestn)
      days = dir(pathstn)
      if(!length(days))
        next
      for(t in 1 : length(days))
      {
        len = str_length(days[t])
        cur_date = substr(days[t],len-13,len-4)
        check_date <- as.Date(Sys.Date(), "%Y-%m-%d")
        check_date = as.character(check_date)
        if(cur_date == check_date)
          next
        if(z == 1)
          daysAlive = daysAlive + 1
        pathread = paste(pathstn,days[t],sep="/")
        pathwritefile = paste(pathwritestn,days[t],sep="/")
        if(RESETHISTORICAL || !file.exists(pathwritefile))
        {
          secondGenData(pathread,pathwritefile,type)
          print(paste(days[t],"Done"))
        }
      }
    }
  }
}
sink()