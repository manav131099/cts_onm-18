constYieldVal = 128.52
timetomins = function(x)
{
	lists = unlist(strsplit(x,"\\ "))
	if(length(lists) < 2)
	return (NA)
	seq1 = seq(from = 2, to = length(lists),by=2)
	lists = lists[seq1]
	lists = unlist(strsplit(lists,":"))
	seq1 = seq(from = 1, to = length(lists),by = 2)
	seq2 = seq(from = 2, to = length(lists),by = 2)
	hr = as.numeric(lists[seq1])
	min = as.numeric(lists[seq2])
	return((hr * 60)+min+1)
}
secondGenData = function(path,pathw)
{
	dataread = read.table(path,header = T,sep = "\t")
	{
	if(nrow(dataread) > 1)
	{
	irr = 0.00
	day = as.character(dataread[1,1])
	day = unlist(strsplit(day,"\\ "))
	day = day[1]
	DA = format(round(nrow(dataread)/2.88,1),nsmall=1)
	ghi = as.numeric(dataread[,4])
	ghi = ghi[complete.cases(ghi)]
	if(length(ghi)>1)
	{
		irr = format(round(sum(ghi)/12000,2),nsmall=2)
	}
	eac = as.numeric(dataread[,3])
	tmstmpo = as.character(dataread[complete.cases(eac),1])
	tmstmpo = tmstmpo[length(tmstmpo)]
	eac = eac[complete.cases(eac)]
	eaclast = format(round(eac[length(eac)],1),nsmall=1)
	stp1 = eac[length(eac)] - eac[1]
	eac = format(round(stp1,1),nsmall=1)
	pac = as.numeric(dataread[,2])
	pac = pac[complete.cases(pac)]
	pac = format(round(sum(pac)/12000,1),nsmall=1)
	tmod = as.numeric(dataread[,5])
	tmod = tmod[complete.cases(tmod)]
	tmod = format(round(mean(tmod),1),nsmall=1)
	tmodsh = timetomins(as.character(dataread[,1]))
	tmodsh2 = dataread[tmodsh > 540,5]
	dataread2 = dataread[tmodsh > 540,]
	tmodsh = tmodsh[tmodsh > 540]
	tmodsh2 = dataread2[tmodsh < 1020,5]
	tmodsh2 = as.numeric(tmodsh2)
	tmodsh = format(round(mean(tmodsh2),1),nsmall=1)
	y1 = format(round(as.numeric(pac)/constYieldVal,2),nsmall=2)
	y2 = format(round(as.numeric(eac)/constYieldVal,2),nsmall = 2)
	pr1 = format(round(as.numeric(pac)*100/(as.numeric(irr)*constYieldVal),1),nsmall=1)
	pr2 = format(round(as.numeric(eac)*100/(as.numeric(irr)*constYieldVal),1),nsmall=1)
	}
	else
	{
	day = DA = irr = pac = eac = y1 = y2 = pr1 = pr2 = tmod = tmodsh = eaclast = tmstmpo = NA
	}
	}
	df = data.frame(Date = day,DA = DA,Irr=irr,EacM1=pac,EacM2=eac,Yield1=y1,Yield2=y2,PR1=pr1,PR2=pr2,Tmod=tmod,TmodSH=tmodsh,EacLast=eaclast,TmLast=tmstmpo)
	write.table(df,file = pathw,row.names = F,col.names = T,sep = "\t",append = F)
}

thirdGenData = function(pathr,pathw)
{
	pathr = read.table(pathr,header = T,sep = "\t")
	if(!file.exists(pathw))
	{
		write.table(pathr,file = pathw,row.names = F,col.names = T,sep = "\t",append = F)
		return()
	}
	write.table(pathr,file = pathw,row.names = F,col.names = F,sep = "\t",append = T)
}
