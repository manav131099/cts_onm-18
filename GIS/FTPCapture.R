errHandle = file('/home/admin/Logs/LogsFTPGIS.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
rm(list = ls())
require('RCurl')
require('utils')
require('mailR')
require('R.utils')
source('/home/admin/CODE/GIS/Aurangabad Totals.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R') 
source('/home/admin/CODE/common/math.R')

print('Historical Calculations Done')

url = "ftp://CLEANTECH:27C0mBA@ftp.geomodel.eu/CLIMDATA/"

citiesName = c("Bhilwara","Chaksu","Chennai","Coimbatore","Lalru","Nasik","Nellore","New Delhi",
"Pune","Lilora","Phnom Penh","Cebu","Manila","Tuas","Woodlands","Ho Chi Minh","Aurangabad","Hyderabad","Ahmedabad","Burhanpur","Navi-Mumbai",
"Satna","Orgadam","Haldia","Bawal")

CONSTANTVALUE = c(2003,1922,1939,1952,1740,1953,1934,1736,1905,1996,1917,1851,1863,1679,1617,1817,1931.2,1960,2022, 1923, 1886,
1874,1943,1739,1799)


while(1)
{
  filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
if(class(filenames)!='try-error')
	break
	print('Error fetching file from FTP... trying again in 60s')
	Sys.sleep(60)
}

sender <- "operations@cleantechsolar.com"
uname <- 'shravan.karthik@cleantechsolar.com'
recipients <- getRecipients("solargis","m") 

passwd = "CTS&*(789"

pth = "/home/admin/Dropbox/GIS/Raw Files"  # Path on server to save file
oldfilenames = dir(pth)
oldfilenameslwr = tolower(oldfilenames)

pthwr = "/home/admin/Dropbox/GIS/Summary"
pthmstr = "/home/admin/Dropbox/GIS/Master"

displaywait=1

siteMapper = c(
"CLEANTECH_Nellore_India_0t_0a",
"CLEANTECH_Lilora_India_0t_0a",
"CLEANTECH_Chennai_India_6t_190a",
"CLEANTECH_Chennai_India_6t_10a",
"CLEANTECH_Chennai_India_12t_180a",
"CLEANTECH_Chennai_India_10t_180a",
"CLEANTECH_Chennai_India_10t_0a",
"CLEANTECH_NewDelhi_India",
"CLEANTECH_Nasik_India",
"CLEANTECH_Bhilwara_India_8t_190a",
"CLEANTECH_Bhilwara_India_8t_10a",
"CLEANTECH_Bhilwara_India_10t_90a",
"CLEANTECH_Bhilwara_India_10t_270a",
"CLEANTECH_Coimbatore_India",
"CLEANTECH_Pune_India_18t_180a",
"CLEANTECH_Pune_India_10t_160a",
"CLEANTECH_Pune_India_22t_180a",
"CLEANTECH_Lalru_India_13t_90a",
"CLEANTECH_Lalru_India_13t_270a",
"CLEANTECH_Lalru_India_3t_270a",
"CLEANTECH_Chaksu_India",
"CLEANTECH_Lilora_India_7t_270a",
"CLEANTECH_Lilora_India_7t_180a",
"CLEANTECH_Lilora_India_7t_90a",
"CLEANTECH_Lilora_India_7t_0a",
"CLEANTECH_Lilora_India_22t_180a",
"CLEANTECH_Bescom_India",
"CLEANTECH_Bhilwara_India_20t_180a",
"CLEANTECH_Lilora_India_8t_180a",
"CLEANTECH_Lilora_India_8t_90a",
"CLEANTECH_Lilora_India_8t_270a",
"CLEANTECH_Lilora_India_8t_0a",
"CLEANTECH_Lilora_India_12t_100a",
"CLEANTECH_Lilora_India_12t_280a",
"CLEANTECH_Lilora_India_22t_180a",
"CLEANTECH_Ho-Chi-Minh_Vietnam_10t_0a",
"CLEANTECH_Ho-Chi-Minh_Vietnam_10t_90a",
"CLEANTECH_Ho-Chi-Minh_Vietnam_10t_180a",
"CLEANTECH_Ho-Chi-Minh_Vietnam_10t_270a",
"CLEANTECH_Aurangabad_India_7t_145a",
"CLEANTECH_Aurangabad_India_7t_325a",
"CLEANTECH_Aurangabad_India_15t_180a",
"CLEANTECH_Aurangabad_India_13t_187a",
"CLEANTECH_Aurangabad_India_15t_187a",
"CLEANTECH_Hyderabad_India_6t_120a",
"CLEANTECH_Hyderabad_India_6t_300a",
"CLEANTECH_Hyderabad_India_15t_180a",
"CLEANTECH_Ahmedabad_India_8t_90a",
"CLEANTECH_Ahmedabad_India_8t_180a",
"CLEANTECH_Ahmedabad_India_8t_0a",
"CLEANTECH_Ahmedabad_India_8t_270a",
"CLEANTECH_Nellore_India_15t_180a",
"CLEANTECH_Burhanpur_India_8t_90a",
"CLEANTECH_Burhanpur_India_8t_270a",
"CLEANTECH_Navi-Mumbai_India_15t_180a",
"CLEANTECH_Navi-Mumbai_India_6t_150a",
"CLEANTECH_Navi-Mumbai_India_6t_330a",
"CLEANTECH_Oragadam_India",
"CLEANTECH_Bawal_India",
"CLEANTECH_Satna_India",
"CLEANTECH_Haldia_India",
"CLEANTECH_Hosur_India",
"CLEANTECH_Khamgaon_India",
"CLEANTECH_Dewas_India",
"CLEANTECH_Jhajjar_India",
"CLEANTECH_Chachoengsao_Thailand_180a",
"CLEANTECH_Chachoengsao_Thailand_0a",
"CLEANTECH_Chonburi_Thailand_180a",
"CLEANTECH_Chonburi_Thailand_0a",
"CLEANTECH_PhnomPenh_Cambodia_0az",
"CLEANTECH_PhnomPenh_Cambodia_90az",
"CLEANTECH_PhnomPenh_Cambodia_180az",
"CLEANTECH_PhnomPenh_Cambodia_270az",
"CLEANTECH_Penang_Malaysia_0az",
"CLEANTECH_Penang_Malaysia_90az",
"CLEANTECH_Penang_Malaysia_180az",
"CLEANTECH_Penang_Malaysia_270az",
"CLEANTECH_Rayong_Thailand_0az",
"CLEANTECH_Rayong_Thailand_180az",
"CLEANTECH_Cebu_Phillipines_0az",
"CLEANTECH_Cebu_Phillipines_90az",
"CLEANTECH_Cebu_Phillipines_180az",
"CLEANTECH_Cebu_Phillipines_270az",
"CLEANTECH_Manila_Phillipines_0az",
"CLEANTECH_Manila_Phillipines_90az",
"CLEANTECH_Manila_Phillipines_180az",
"CLEANTECH_Manila_Phillipines_270az",
"CLEANTECH_CMIC_Cambodia_0az",
"CLEANTECH_CMIC_Cambodia_90az",
"CLEANTECH_CMIC_Cambodia_180az",
"CLEANTECH_CMIC_Cambodia_270az",
"CLEANTECH_SamutSakhon_Thailand_0az",
"CLEANTECH_SamutSakhon_Thailand_90az",
"CLEANTECH_SamutSakhon_Thailand_180az",
"CLEANTECH_SamutSakhon_Thailand_270az",
"CLEANTECH_MPIP_Vietnam",
"CLEANTECH_Tay-Ninh_Vietnam_10t_0a",
"CLEANTECH_Tay-Ninh_Vietnam_10t_90a",
"CLEANTECH_Tay-Ninh_Vietnam_10t_180a",
"CLEANTECH_Tay-Ninh_Vietnam_10t_270a",
"CLEANTECH_Ahmednagar_India",
"CLEANTECH_Karaikal_India",
"CLEANTECH_Samutskrm_Thailand",
"CLEANTECH_Samphran_Thailand",
"CLEANTECH_Sukha_Thailand",
"CLEANTECH_Bangpain_Thailand",
"CLEANTECH_Rojana_Thailand",
"CLEANTECH_Suphan_Thailand",
"CLEANTECH_Phitlok_Thailand",
"CLEANTECH_Uttaradit_Thailand",
"CLEANTECH_Phrae_Thailand",
"CLEANTECH_Nan_Thailand",
"CLEANTECH_Chiangkm_Thailand",
"CLEANTECH_Chiangkng_Thailand",
"CLEANTECH_Tirunelveli_India",
"CLEANTECH_Kuranganwali_India",
"CLEANTECH_Lopburi_Thailand",
"CLEANTECH_Korat_Thailand",
"CLEANTECH_Chumphon_Thailand",
"CLEANTECH_Prachuap_Thailand",
"CLEANTECH_Thayang_Thailand",
"CLEANTECH_Kampngsaen_Thailand",
"CLEANTECH_Kan_Thailand",
"CLEANTECH_Maesot_Thailand",
"CLEANTECH_Tak_Thailand",
"CLEANTECH_Enstek_Malaysia_0az",
"CLEANTECH_Enstek_Malaysia_90az",
"CLEANTECH_Enstek_Malaysia_180az",
"CLEANTECH_Enstek_Malaysia_270az",
"CLEANTECH_ChangiAirport_Singapore_0az",
"CLEANTECH_ChangiAirport_Singapore_90az",
"CLEANTECH_ChangiAirport_Singapore_180az",
"CLEANTECH_ChangiAirport_Singapore_270az",
"CLEANTECH_Tuas_Singapore_0az",
"CLEANTECH_Tuas_Singapore_90az",
"CLEANTECH_Tuas_Singapore_180az",
"CLEANTECH_Tuas_Singapore_270az",
"CLEANTECH_Woodlands_Singapore_0az",
"CLEANTECH_Woodlands_Singapore_90az",
"CLEANTECH_Woodlands_Singapore_180az",
"CLEANTECH_Woodlands_Singapore_270az",
"CLEANTECH_Taiping_Malaysia_0az",
"CLEANTECH_Taiping_Malaysia_90az",
"CLEANTECH_Taiping_Malaysia_180az",
"CLEANTECH_Taiping_Malaysia_270az",
"CLEANTECH_Tangkak_Malaysia",
"CLEANTECH_NegeriPerak_Malaysia",
"CLEANTECH_Paka_Malaysia",
"CLEANTECH_TanjungPelepas_Malaysia",
"CLEANTECH_Gudang_Malaysia"
)

expungeFromNames = c(
NA,
"0t_0a_",
"6t_190a_",
"6t_10a_",
"12t_180a_",
"10t_180a_",
"10t_0a_",
NA,
NA,
"8t_190a_",
"8t_10a_",
"10t_90a_",
"10t_270a_",
NA,
"18t_180a_",
"10t_160a_",
"22t_180a_",
"13t_90a_",
"13t_270a_",
"3t_270a_",
NA,
"7t_270a_",
"7t_180a_",
"7t_90a_",
"7t_0a_",
"22t_180a_",
NA,
"20t_180a_",
"8t_180a_",
"8t_90a_",
"8t_270a_",
"8t_0a_",
"12t_100a_",
"12t_280a_",
"22t_180a_",
"10t_0a_",
"10t_90a_",
"10t_180a_",
"10t_270a_",
"7t_145a_",
"7t_325a_",
"15t_180a_",
"13t_187a_",
"15t_187a_",
"6t_120a_",
"6t_300a_",
"15t_180a_",
"8t_90a_",
"8t_180a_",
"8t_0a_",
"8t_270a_",
"15t_180a_",
"8t_90a_",
"8t_270a_",
"15t_180a_",
"6t_150a_",
"6t_330a_",
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
"0az_Cambodia_",
"90az_Cambodia_",
"180az_Cambodia_",
"270az_Cambodia_",
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"180az_",
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"90az_",
"180az_",
"270az_",
NA,
"10t_0a_",
"10t_90a_",
"10t_180a_",
"10t_270a_",
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
NA,
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"90az_",
"180az_",
"270az_",
"0az_",
"90az_",
"180az_",
"270az_",
NA,
NA,
NA,
NA,
NA
)

updateName = function(x)
{
	if(grepl("Solargis",x))
  {
    x = gsub("Solargis","SolarGIS",x)
  }
  if(grepl("Ho_Chi_Minh",x))
	{
		x = gsub("Ho_Chi_Minh","Ho-Chi-Minh",x)
	}
	if(grepl("Navi_Mumbai",x))
	{
		x = gsub("Navi_Mumbai","Navi-Mumbai",x)
	}
	if(grepl("BandarBaruEnstek",x))
	{
		x = gsub("BandarBaruEnstek","Enstek",x)
	}
	if(grepl("Khambaba_Rd",x))
	{
		x = gsub("Khambaba_Rd","Satna",x)
	}
	if(grepl("Asalwas",x))
	{
		x = gsub("Asalwas","Bawal",x)
	}
  
  if(grepl("Tambon_Khlong_Chik",x))
  {
    x = gsub("Tambon_Khlong_Chik","Bangpain",x)
  }
  if(grepl("Tambon_Khan_Ham",x))
  {
    x = gsub("Tambon_Khan_Ham","Rojana",x)
  }
  if(grepl("Tambon_Tha_Rahat",x))
  {
    x = gsub("Tambon_Tha_Rahat","Suphan",x)
  }
  if(grepl("Tambon_Tha_It",x))
  {
    x = gsub("Tambon_Tha_It","Uttaradit",x)
  }
  if(grepl("Tambon_Du_Tai",x))
  {
    x = gsub("Tambon_Du_Tai","Nan",x)
  }
  if(grepl("Tambon_Wiang",x))
  {
    x = gsub("Tambon_Wiang","Wiang-Tambon",x)
  }

	if(grepl("Cleantech_Solar_",x))
	{
		getnum = unlist(strsplit(x,"_"))
		getnum = getnum[6]
		x = gsub(paste("Cleantech_Solar_",getnum,sep=""),"CLEANTECH",x)
	}
	if(grepl("NR31road",x))
	{
		x = gsub("NR31road","CMIC",x)
	}
	if(grepl("NR_31_road",x))
	{
		x = gsub("NR_31_road","CMIC",x)
	}
	if(grepl("Kamunting",x))
	{
		x = gsub("Kamunting","Taiping",x)
	}
	if(grepl("Samut_Sakhon",x))
	{
		x = gsub("Samut_Sakhon","SamutSakhon",x)
	}
	if(grepl("Chang_Wat_Rayong",x))
	{
		x = gsub("Chang_Wat_Rayong","Chonburi",x)
	}
	if(grepl("My_Phuoc_2_Industrial_park",x))
	{
		x = gsub("My_Phuoc_2_Industrial_park","MPIP",x)
	}
	if(grepl("Tay_Ninh",x))
	{
		x = gsub("Tay_Ninh","Tay-Ninh",x)
	}
  
  if(grepl("TambonMaeSot",x))
  {
    x = gsub("TambonMaeSot","Maesot",x)
  }
  if(grepl("TambonPak",x))
  {
    x = gsub("TambonPak","Kan",x)
  }
  if(grepl("TambonRahaeng",x))
  {
    x = gsub("TambonRahaeng","Tak",x)
  }
  if(grepl("TambonThung",x))
  {
    x = gsub("TambonThung","Kampngsaen",x)
  }
  if(grepl("TambonWangPhai",x))
  {
    x = gsub("TambonWangPhai","Chumphon",x)
  }
  if(grepl("Amphoe",x))
  {
    x = gsub("Amphoe","Prachuap",x)
  }
  if(grepl("_Tambon_",x))
  {
    x = gsub("_Tambon_","_Thayang_",x)
  }
  if(grepl("_Sot_Thailand_",x))
  {
    x = gsub("_Sot_Thailand","",x)
  }
  if(grepl("_Phai_Thailand_",x))
  {
    x = gsub("_Phai_Thailand","",x)
  }

	retval = x
	splitvals = unlist(strsplit(as.character(x),"_"))
	if(is.finite(as.numeric(splitvals[3])))
	{
			  if(as.numeric(splitvals[3]) == 22)
				{
					if(grepl("Karnataka",as.character(x)))
					{
						x = gsub("_22_","_27_",x)
						splitvals[3] = "27"
					}
				}
				if(!is.na(expungeFromNames[as.numeric(splitvals[3])]))
				{
					print(paste("substituting",expungeFromNames[as.numeric(splitvals[3])],"from",x))
					x = gsub(expungeFromNames[as.numeric(splitvals[3])],"",x)
				}
				retval = gsub(paste("_",splitvals[3],"_",splitvals[4],"_",splitvals[5],"_",sep=""),
				paste("_",siteMapper[as.numeric(splitvals[3])],"_",sep=""),x)
				splitvals = unlist(strsplit(as.character(retval),"_"))
			  splitvals[3:(length(splitvals)+1)] = splitvals[2:length(splitvals)]
				splitvals[2]="TS"
				if(substr(splitvals[(length(splitvals)-1)],1,2)!='20')
				{
				splitvals[(length(splitvals)+1)] = splitvals[length(splitvals)]
				splitvals[(length(splitvals)-1)] = substr(splitvals[length(splitvals)],1,8)
				}
				retval = paste(splitvals,collapse="_")
	}
	strtdate = splitvals[(length(splitvals)-1)]
	enddate = splitvals[length(splitvals)]
	enddate = gsub(".csv","",enddate)
	addz = 0
	if((strtdate != substr(enddate,1,8)) && (!grepl("ZSolar",x)))
	{
		print(paste("Adding z to",retval,"since startdte is",strtdate,"and enddate is",substr(enddate,1,8)))
		addz = 1
	}
	if(addz)
		retval = paste("Z",retval,sep="")
  if(grepl("Thailand_Thailand",retval))
  {
    retval = gsub("Thailand_Thailand","Thailand",retval)
  }
	return(retval)
}

isTime7AMSG = function()
{
	sgTime = as.numeric(substr(format(Sys.time(),tz="Singapore",usetz=T),12,13))
	if(sgTime > 7 && sgTime < 15)
	{
		return(1)
	}
	else
	return(0)
}

LASTTIMESTAMPOLD = LASTTIMESTAMP = c()

recordTime = function()
{
	timeset = c()
	idx = 1
	for(x in 1 : length(cumnames))
	{
			if(grepl("Bratislava",cumnames[x]) || (grepl("TAY-",cumnames[x])))
				next
			dataread = try(read.table(cumnames[x],header = T,sep = "\t"),silent=T)
			if(class(dataread)=='try-error')
				next
			value = as.character(dataread[nrow(dataread),1])
			timeset[idx] = value
			idx = idx + 1
	}
	return(timeset)
}

sendToken = 0
forceSendToken = 1

LASTTIMESTAMPOLD = recordTime()

OLDPARSEENABLE = 0
while(1)
{
	recipients <- getRecipients("solargis","m") 
  print(paste('Still alive',format(Sys.time(),tz="Asia/Calcutta")))
  filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE,timeout = 600,ssl.verifypeer = FALSE,useragent="R"),
	 timeout = 600, onTimeout = "error"),silent=T)
	if(class(filenames)=='try-error')
	{
		print('Error fetching file from FTP... trying again in 60s')
		Sys.sleep(60)
		next
	}
  newfilenames = try(unlist(strsplit(filenames,"\n")),silent=T)
	if(class(filenames)=='try-error')
	{
		print('Error spliting filenames... trying again in 60s')
		Sys.sleep(60)
		next
	}
	recordTimeMaster("GIS","FTPProbe")
	newfilenamesorig = newfilenames
	filespath = cumnames
  filesname = filenamestoattach
  update = oldupdate = length(filespath)
	if(OLDPARSEENABLE)
	{
		idxmtchnf = match(oldnewfilenames,newfilenames)
		idxmtchnf = idxmtchnf[complete.cases(idxmtchnf)]
		newfilenames = newfilenames[-idxmtchnf]
		if(!length(newfilenames))
		{
			print('No New files')
			Sys.sleep(600)
			next
		}
	}
  print(paste("There are new files in the pipeline and NY time now is",Sys.time()))
  for(x in 1 : length(newfilenames))
  {
			urlfilename = newfilenames[x]
	    if(grepl('Coimbatore',newfilenames[x]))
			{
			  newfilenames[x] = gsub("_12t","",newfilenames[x])
			  newfilenames[x] = gsub("_180a","",newfilenames[x])
			}
			if(grepl('Singapore_Singapore',newfilenames[x]))
			{
				next
			}
			if(grepl('Bhilwara',newfilenames[x]))
			{
				if(length(unlist(strsplit(newfilenames[x],"_")))<10)
					{
					vals = unlist(strsplit(newfilenames[x],"_"))
					if(!is.finite(as.numeric(vals[3])))
						next
					}
			}
  {
      if(is.finite(match(newfilenames[x],oldfilenames)) || grepl("DataRequest",newfilenames[x]))
    {
      print('Hitting next')
			next
    }
    if(newfilenames[x] ==  "Solargis_TS_hourly_CLEANTECH_102_Karaikal_20190101_20191201.csv" || 
    newfilenames[x] == "Solargis_TS_hourly_CLEANTECH101_Ahmednagar_20190101_20191201.csv")
      next
    else
    {
	if(!(grepl("csv",newfilenames[x])))
	{
		if(displaywait == 1){
		print("Skipping PDF or HTML files")}
		next
	}
			if(is.finite(match(updateName(newfilenames[x]),oldfilenames)))
				next
			print(paste('New file found',newfilenames[x]))
      {
				urlnew = paste(url,urlfilename,sep="")
     	}
			newfilenames[x] = updateName(newfilenames[x])
			isexist = match(newfilenames[x],oldfilenames)
			print(paste("Newfilenames is",newfilenames[x],"and isexist is",isexist))
			if(is.finite(isexist))
				next
			isexist = match(tolower(newfilenames[x]),oldfilenameslwr)
			if(is.finite(isexist))
				next
      file = try(download.file(urlnew,destfile = paste(pth,newfilenames[x],sep="/")),silent=T)
			if(class(file)=='try-error')
			{
				print(paste('Error in downloading file',paste(pth,newfilenames[x],sep="/")))
				next
			}
      oldfilenames[length(oldfilenames)+1] = newfilenames[x]
			dataintermediate = try(read.table(paste(pth,newfilenames[x],sep="/"),header = T,sep = ";"),silent=T)
			if(class(dataintermediate)=='try-error')
				next
      update = update = update + 1
      filespath[update] = calcSummary(paste(pth,newfilenames[x],sep="/"))
			recordTimeMaster("GIS","FTPNewFiles",newfilenames[x])
      print(paste('Summary Calculated for',newfilenames[x]))
      int = unlist(strsplit(filespath[update],"/"))
      filesname[update] = int[length(int)]
			update = length(unique(filespath))
			filespath = unique(filespath)
			filesname = unique(filesname)
			Sys.sleep(10)
    }
  }
  }
	OLDPARSEENABLE = 1
	oldnewfilenames = newfilenamesorig
  if(update > oldupdate)
  {
		sendToken = 1
    print("Received new files, so updating sendtoken to true")
	}
  print("Checking if mail has to be sent")
  print(paste("SG time now is",format(Sys.time(),tz="Singapore",usetz=T)))
  print(paste("forcesendtoken is",forceSendToken,"isTime7AMSG is",isTime7AMSG(),"sendToken is",sendToken))
	if((isTime7AMSG() && sendToken) || forceSendToken)
	{
  displaywait = 1
  #currdate = seq(as.Date(Sys.Date()), length = 2, by = "-1 months")[2]
  currdate = as.character(Sys.Date())
	oldupdate = update
	removerow = NULL
	pathwrite = '/home/admin/Dropbox/GIS/Summary'
  checkAggregate(pathwrite)
	body = c()
	lstDate = c()
	valIrr = c()
	idxBody = 1
	COVVARIND = c()
	COVARHUB = c()
	for(x in 1 : length(filespath))
	{
		{
		if(!file.exists(filespath[x]))
		{
			removerow = rbind(removerow,x)
		}
		else
		{
			if(!grepl("Aggregate",filespath[x]))
			next
			dataread = read.table(filespath[x],header = T,sep = "\t",stringsAsFactors=F)
			value = as.character(dataread[nrow(dataread),1])
			lstDate[idxBody] = value
			ma30 = tail(as.numeric(dataread[,2]),30)
			ma30 = ma30[complete.cases(ma30)]
			ma30 = format(round(mean(ma30),2),nsmall=2)
			ma365 = tail(as.numeric(dataread[,2]),365)
			ma365 = ma365[complete.cases(ma365)]
			ma365 = format(round((sum(ma365)*365/max(length(ma365),1)),1),nsmall=1)
			
			ghi = as.character(dataread[nrow(dataread),2])
			valIrr[idxBody] = as.numeric(ghi)
			name = unlist(strsplit(filespath[x],"/"))
			name = name[length(name)-1]
			{
			if(name=='NewDelhi')
				name = "New Delhi"
			else if(name=='PhnomPenh')
				name = "Phnom Penh"
			else if (name=='Bratislava')
				next
			else if (name=='Tay-Ninh')
				next
			else if(name=="ChangiAirport")
				name = "Changi Airport"
			else if(name == "Ho-Chi-Minh")
				name = "Ho Chi Minh"
      }
			idxconstant = match(name,citiesName)
			if(!is.finite(idxconstant))
				idxconstant = 1
			const = round((((as.numeric(ma365)/CONSTANTVALUE[idxconstant]) - 1)*100),1)
			country = '(IN)'
			{
				if(name=='Tuas' || name == 'Woodlands' || name == "Changi Airport")
					country = '(SG)'
				else if(name == 'Phnom Penh')
					country = '(KH)'
				else if(name == 'Manila')
					country = '(PH)'
				else if (name == 'Cebu')
					country = '(PH)'
				else if (name == 'Penang' || name == "Enstek" || name == 'TanjungPelepas' ||
                 name == 'NegeriPerak' || name == 'Gudang' || name == 'Tangkak' ||
                 name == 'Paka')
					country = '(MY)'
				else if (name == 'Rayong' || name == 'Chachoengsao' || name == 'Chonburi'  ||
          name == 'Uttaradit' || name == 'Tak' || name == 'Chiangkng' ||
          name == 'Thayang' || name == 'Phitlok' || name == 'Maesot' || 
          name == 'Suphan' || name == 'Prachuap' || name == 'Nan' || 
          name == 'Rojana' || name == 'Chiangkm' || name == 'Bangpain' || 
          name == 'Korat' || name == 'Phrae' || name == 'Samphran' || 
          name == 'Samutskrm' || name == 'Chumphon' || name == 'Sukha')
					country = '(TH)'
				else if (name == "Ho Chi Minh")
					country = "(VN)"
				else if(name == "Taiping")
					country = "(MY)"
				else if(name == "SamutSakhon")
					country = "(TH)"
				else if(name == "CMIC")
					country = "(KH)"
				else if(name == "MPIP" || name == "Tay-Ninh")
					country = "(VN)"
			}
			#int = unlist(strsplit(fil,"/"))
			if(country == '(IN)' || country == '(VN)' || country == '(TH)' || country == "(KH)")
			{
				value = paste(value,", ",ghi," kWh/m2",sep="")
				if(name == "Bescom" || name == "Bhilwara" || name == "Aurangabad" || name == "Hyderabad" || name == "Ahmedabad")
				{
					colnogti = 3
					if(name == "Bhilwara")
						colnogti = 5
						#colnogti
					if(name == "Aurangabad" || name == "Hyderabad" || name == "Ahmedabad")
						colnogti = 4
					gti = as.character(dataread[nrow(dataread),colnogti])
					diff = round((((as.numeric(gti)-as.numeric(ghi))/as.numeric(ghi))*100),1)
					value = paste(value,", GTI: ",gti," kWh/m2 (",diff,"%)",sep="")
				}
			}
			if(country == '(IN)')
				COVVARIND = c(COVVARIND,as.numeric(ghi))
			if(name == "New Delhi" || name == "Pune" || name == "Chennai" || name == "Ahmedabad" || name == "Chaksu" || name == "Bescom" || name == "Hyderabad")
				COVARHUB = c(COVARHUB,as.numeric(ghi))
			body[idxBody] = paste(name," ",country,": ",value,", 30-d MA: ",ma30,", 365-d sum: ",ma365,", ",const,"% vs. TMY\n\n",sep="")
			idxBody = idxBody + 1
		}
		}
	}
	if(length(removerow))
	{
		print(paste('remove rows',removerow))
		filespath = filespath[-removerow]
		filesname = filesname[-removerow]
	}
	bodytemp = body
	print("..................BEFORE ORDER...................")
	print(lstDate)
	print(valIrr)
	print(order(lstDate,valIrr,decreasing=T))
	print("............DONE....")
	body = body[order(lstDate,valIrr,decreasing = T)]
	body = paste(body,collapse="")
	#bodysplit = try(unlist(strsplit(body,"\n\n")),silent=T)
	LASTTIMESTAMP = recordTime()
	if(F){
	placeEqual = which((LASTTIMESTAMP==LASTTIMESTAMPOLD) %in% TRUE)
	print(paste('Index where equal',placeEqual))
	if(length(placeEqual))
	{
		equaltimes = LASTTIMESTAMP[placeEqual]
		equaltimes = rev(order(equaltimes))
		placeEqual = match(equaltimes[1],LASTTIMESTAMP)
	}
	if(class(bodysplit)!='try-error')
	{
		vals = unlist(strsplit(bodysplit,":"))
		seq1 = seq(from=2,to=length(vals),by=2)
		orderrowsac = rev(order(vals[seq1]))
		print(paste('Length of placeEqual',length(placeEqual)))
		if(length(placeEqual))
		{
			print(paste('Index where equal',placeEqual))
			bodysplit[placeEqual] = paste(bodysplit[placeEqual],"\n-----------------------------------------------",sep="")
		}
		bodysplit = bodysplit[orderrowsac]
		body = paste(bodysplit,collapse="\n\n") 
	}
	}
	filespath = filespath[grepl("Aggregate",filespath)]
	filesname = filesname[grepl("Aggregate",filesname)]
	idxexclude = c()
	idxouter = 1
	for(t in 1 : length(filespath))
	{
		if(!file.exists(filespath[t]) || grepl("BRAT",filespath[t]))
		{
			idxexclude[idxouter] = t
			idxouter = idxouter + 1
		}
	}
	if(idxouter > 1)
	{
		filespath = filespath[-idxexclude]
		filesname = filesname[-idxexclude]
	}
	print(filespath)
	print("----------")
	print(filesname)
	lengthOfNewDays = length(filespath)
	bodyHead = paste("Last date received for",lengthOfNewDays,
	"stations and daily irradiation for the given day:")
	body = paste( bodyHead,body,sep="\n\n")
	body = paste(body,"**************************",sep="")
	COVVARIND = COVVARIND[complete.cases(COVVARIND)]
	COVARHUB = COVARHUB[complete.cases(COVARHUB)]
	body = paste(body,"\n\nAverage GHI Indian cities [kWh/m2]:",round(mean(COVVARIND),2))
	body = paste(body,"\n\nSD GHI Indian cities [kWh/m2]:",round(sdp(COVVARIND),2))
	body = paste(body,"\n\nCOV GHI Indian cities [%]:",round(sdp(COVVARIND)*100/mean(COVVARIND),2))
	body = paste(body,"\n\n**************************",sep="")
	body = paste(body,"\n\nAverage GHI Hub Indian cities [kWh/m2]:",round(mean(COVARHUB),2))
	body = paste(body,"\n\nSD GHI Hub Indian cities [kWh/m2]:",round(sdp(COVARHUB),2))
	body = paste(body,"\n\nCOV GHI Hub Indian cities [%]:",round(sdp(COVARHUB)*100/mean(COVARHUB),1))
	body = paste(body,"\n\n**************************",sep="")
	body = paste(body,
	"\n\n New Delhi (1.9 MWp), Pune (3.5 MWp), Chennai (5.2 MWp), Ahmedabad (19.6 MWp), Chaksu (1.7 MWp), Bescom (4.5 MWp) and Hyderabad (0.9 MWp) - 37.3 out of 51.4 MWp (73% of India portfolio))")
	filespath[(length(filespath)+1)] = paste(pthmstr,"Z_AllSites_Master.txt",sep="/")
	filesname[(length(filesname)+1)] = "Z_AllSites_Master.txt"
	filesnamecpy = filesname[order(filesname)]
	idxs = match(filesnamecpy,filesname)
	filespath = filespath[idxs]
	filesname = filesname[idxs]
	body = gsub("\n ","\n",body)
  computeMaster()
	command = paste("Rscript '/home/admin/CODE/Misc/GIS_wsVN.R'")
	system(command) 
	command = paste("Rscript '/home/admin/CODE/Misc/GIS_wsCM.R'")
	system(command) 
	command = paste("Rscript '/home/admin/CODE/Misc/GIS_wsTHCH.R'")
	system(command) 
	
	filespath = c(filespath,"/tmp/HO-C_WS.txt","/tmp/COIM_WS.txt","/tmp/THChumphon_WS.txt")
	filesname = c(filesname,"HO-C_WS.txt","COIM_WS.txt","CHUMP_WS.txt")
	send.mail(from = sender,
            to = recipients,
            subject = paste("GIS Files Update",substr(currdate,1,10)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = passwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filespath,
            file.names = filesname, # optional parameter
            debug = F)
	recordTimeMaster("GIS","Mail",substr(currdate,1,10))
  print("Mail Sent")
  	inject_command="python3 /home/admin/CODE/Misc/Auto_Inject.py"
	#system(inject_command)
	LASTTIMESTAMPOLD = LASTTIMESTAMP
	gc()
	sendToken=0
	forceSendToken = 0
  }
 if(displaywait == 1){
  print("Waiting")
displaywait = 0}
  Sys.sleep(3600)
}
sink()
