#source("/home/admin/Jason/cec intern/codes/711/711 SMP10 data extract for irradiance fitting.R")
rm(list=ls(all =TRUE))
library(ggplot2)
source('/home/admin/CODE/common/math.R')

i = "/home/admin/Jason/cec intern/results/711/[711]_da_summary.txt"
j = "/home/admin/Dropbox/GIS/Summary/NewDelhi/NEWD Aggregate.txt"     #2017-12-28 remove

temp1 <- read.table(i,header = T, sep = "\t")
temp1 <- temp1[-(1:15),]
temp2 <- read.table(j,header = T, sep = "\t")
temp3 <- as.data.frame(cbind(temp1[1:length(temp2[,1]),],temp2))

gsiratio <- temp3[,3]/temp3[,5]

temp4 <- data.frame(Date =0,
                    DA = temp3[,2],
                    SMP10 = temp3[,3],
                    GHI = temp3[,5])

temp4$Date = temp3[,1]
temp4$Month <- as.factor(substr(temp4$Date,3,7))
model <- lm(temp4[,3]~temp4[,2])

rsq <- format(as.numeric(summary(model)$r.squared),digits=3,nsmall = 3)

sd1 <- sdp(temp4[,2], na.rm = T)
sd2 <- sdp(temp4[,3], na.rm = T)

p1 <- ggplot(temp4, aes(x=SMP10,y=GHI,group=Month))
p1 <- p1 + geom_point(aes(shape = Month,colour = Month))
p1 <- p1 + scale_shape_manual(values=seq(0,(nlevels(temp4[,5])-1)))
p1 <- p1 + theme_bw()
p1 <- p1 + geom_abline(intercept = 0, slope = 1)
p1 <- p1 + expand_limits(x=0,y=0)
p1 <- p1 + coord_cartesian(ylim=c(0,8),xlim=c(0,8))
p1 <- p1 + xlab(expression(paste("Ground Global Horizontal Irradiation [",kwh/m^2,", daily]")))
p1 <- p1 + ylab(expression(paste("Satellite Global Horizontal Irradiation [",kwh/m^2,", daily]")))
p1 <- p1 + ggtitle(paste("Satellite vs. 711 Ground Irradiation - New Delhi, India "))
p1 <- p1 + annotate("text",label = paste0("R-sq error:",rsq),size=3.6, x = 4, y= 1)
p1 <- p1 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.8,hjust = 0.5))

p1

ggsave(p1, filename = paste0("/home/admin/Jason/cec intern/results/711/711 vs sat(NEWD).pdf"),width = 7.92, height = 5)

write.table(temp4,na = "", "/home/admin/Jason/cec intern/results/711/[711]_smp10_summary.txt",row.names = FALSE,sep ="\t")

source("/home/admin/CODE/JasonGraphs/711/711 monthly SMP10 vs NEWD sate graph.R")

