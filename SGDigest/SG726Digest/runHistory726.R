rm(list = ls(all = T))
source('/home/admin/CODE/common/math.R')
INSTCAP = c(819,410.80)
DAYSACTIVE = 0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}

fetchGSIData = function(date,wait)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[SG-724S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[SG-724S] ',date,".txt",sep="")
	print(txtFileName)
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,3])
			}
		}
	}
	if(wait && (!is.finite(gsiVal)))
	{
		print("Couldnt find file, sleeping for an hour")
		Sys.sleep(3600)
		gsiVal = fetchGSIData(date,0)
	}
	return(gsiVal)
}

prepareSumm = function(dataread1, wait, dataread2=NA)
{
  da = nrow(dataread1)
  daPerc = round(da/14.4,1)
	thresh = 5
	LastRead1 = LastTime1 = Eac11 = Eac21 = Eac31 = GA71 = PA71 = GA7 = PA7 = NA
  LastRead2 = LastTime2 = Eac12 = Eac22 = Eac32 = GA72 = PA72 = NA
	
	date = substr(as.character(dataread1[1,1]),1,10)
	Eac11 = as.numeric(dataread1[,16])
	Eac11 = Eac11[complete.cases(Eac11)]
	
	{
	if(length(Eac11))
		Eac11 = sum(Eac11)/60
	else
		Eac11 = NA
	}

	Eac21 = as.numeric(dataread1[,31])
	time = as.character(dataread1[,1])
	time = time[complete.cases(Eac21)]
	Eac21 = Eac21[complete.cases(Eac21)]
	cond = (Eac21 > 0 & Eac21 < 1000000000)
	Eac21 = Eac21[cond]
	{
	if(length(Eac21))
	{
		LastRead1 = Eac21[length(Eac21)]
		Eac21 = (Eac21[length(Eac21)] - Eac21[1])
		LastTime1 = time[length(time)]
	}
	else
		Eac21 = NA
	}
	
	Eac31 = as.numeric(dataread1[,32])
	Eac31 = Eac31[complete.cases(Eac31)]
	{
		if(length(Eac31))
			Eac31 = Eac31[length(Eac31)]
		else
			Eac31 = NA
	}
	
  Eac12 = as.numeric(dataread1[,63])
	Eac12 = Eac12[complete.cases(Eac12)]
	
	{
	if(length(Eac12))
		Eac12 = sum(Eac12)/60
	else
		Eac12 = NA
	}
	
  Eac22 = as.numeric(dataread1[,78])
	time = as.character(dataread1[,1])
	time = time[complete.cases(Eac22)]
	Eac22 = Eac22[complete.cases(Eac22)]
	cond = (Eac22 > 0 & Eac22 < 1000000000)
	Eac22 = Eac22[cond]
	{
	if(length(Eac22))
	{
		LastRead2 = Eac22[length(Eac22)]
		Eac22 = (Eac22[length(Eac22)] - Eac22[1])
		LastTime2 = time[length(time)]
	}
	else
		Eac22 = NA
	}
	
	Eac32 = as.numeric(dataread1[,79])
	Eac32 = Eac32[complete.cases(Eac32)]
	{
		if(length(Eac32))
			Eac32 = Eac32[length(Eac32)]
		else
			Eac32 = NA
	}
	
	Yld11 = Eac11/INSTCAP[1]
	Yld21 = Eac21/INSTCAP[1]
	IrrSG724 = fetchGSIData(date,wait)
	PR11 = Yld11*100/IrrSG724
	PR21 = Yld21*100/IrrSG724
	
  Yld12 = Eac12/INSTCAP[2]
	Yld22 = Eac22/INSTCAP[2]
	PR12 = Yld12*100/IrrSG724
	PR22 = Yld22*100/IrrSG724
  
  if (length(dataread2) > 1)
  {	
    timestamps_irradiance_greater_20 = dataread2[dataread2[, 4]>20, 1]	
    
    timestamps_freq_greater_40 = dataread1[(dataread1[,29]>40 & dataread1[,29]!= "NaN") ,1]	
    timestamps_pow_greater_2 = dataread1[abs(dataread1[,16])>2,1]	
    common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)	
    common2 = intersect(common, timestamps_pow_greater_2)	
    GA71 = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)	
    PA71 = round(((length(common2)/length(common))*100), 1)
    
    timestamps_freq_greater_40 = dataread1[(dataread1[,76]>40 & dataread1[,76]!= "NaN") ,1]	
    timestamps_pow_greater_2 = dataread1[abs(dataread1[,63])>2,1]	
    common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)	
    common2 = intersect(common, timestamps_pow_greater_2)	
    GA72 = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)	
    PA72 = round(((length(common2)/length(common))*100), 1)
    
    GA7 = round(((GA71 * INSTCAP[1] + GA72 * INSTCAP[2])/sum(INSTCAP)), 1)
    PA7 = round(((PA71 * INSTCAP[1] + PA72 * INSTCAP[2])/sum(INSTCAP)), 1)
    
  }
  
  
  Eac1Tot = Eac11 + Eac12 
  Eac2Tot = Eac22 + Eac21 
  Yld1Tot = Eac1Tot /sum(INSTCAP)
  Yld2Tot = Eac2Tot /sum(INSTCAP)
  PR1Tot = Yld1Tot*100/IrrSG724
  PR2Tot = Yld2Tot*100/IrrSG724

	datawrite = data.frame(Date = date, PtsRec = da, DA = rf1(daPerc), 
  Eac11 = rf(Eac11), Eac21 = rf(Eac21),
	Yld11 = rf(Yld11), Yld21 = rf(Yld21), 
  GSiSG724 = IrrSG724, PR11 = rf1(PR11), PR21 = rf1(PR21),
	LastTime1 = LastTime1, LastRead1 = LastRead1, LastReadRec1 = Eac31, 
  Eac12 = rf(Eac12), Eac22 = rf(Eac22),
	Yld12 = rf(Yld12), Yld22 = rf(Yld22), 
  PR12 = rf1(PR12), PR22 = rf1(PR22),
	LastTime2 = LastTime2, LastRead2 = LastRead2, LastReadRec2 = Eac32,
  Eac1Tot = rf(Eac1Tot), Eac2Tot = rf(Eac2Tot), Yld1Tot = rf(Yld1Tot), 
  Yld2Tot=rf(Yld2Tot), PR1Tot=rf1(PR1Tot), PR2Tot=rf(PR2Tot), 
  GA71 = GA71, PA71 = PA71, GA72 = GA72, PA72 = PA72, GA7 = GA7, PA7 = PA7, 
  stringsAsFactors=F)

  datawrite 
}

rewriteSumm = function(datawrite)
{
  datawrite
}

path1 = "/home/admin/Dropbox/Cleantechsolar/1min/[726]"
path2 = "/home/admin/Dropbox/Cleantechsolar/1min/[724]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-726S]"
checkdir(pathwrite)
years1 = dir(path1)
x=y=z=1
for(x in 1 : length(years1))
{
  pathyear1 = paste(path1,years1[x],sep="/")
 	pathyear2 = paste(path2,years1[x],sep="/")
  writeyear = paste(pathwrite,years1[x],sep="/")
  checkdir(writeyear)
  months1 = dir(pathyear1)
  for(y in  1: length(months1))
  {
    pathmonth1 = paste(pathyear1,months1[y],sep="/")
    pathmonth2 = paste(pathyear2,months1[y],sep="/")
    writemonth = paste(writeyear,months1[y],sep="/")
    checkdir(writemonth)
    days1 = dir(pathmonth1)
    sumfilename = paste("[SG-726S] ",substr(months1[y],3,4),substr(months1[y],6,7),".txt",sep="")
    for(z in 1 : length(days1))
    {
      dataread1 = read.table(paste(pathmonth1,days1[z],sep="/"),sep="\t",header = T)
      fname = paste("[724]", substr(days1[z],7,20),sep=" ")	
      if(file.exists(paste(pathmonth2,fname,sep="/")))	
        dataread2 = read.table(paste(pathmonth2,fname,sep="/"),sep="\t",header = T)	
      else	
        dataread2 = NA
      datawrite = prepareSumm(dataread1, 0, dataread2) #dont wait for history
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("726","SG-726S",days1[z])
			if(x == 1 && y == 1 && z == 1)
				DOB = substr(days1[z],10,19)
			DAYSACTIVE = DAYSACTIVE + 1
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
