rm(list = ls())
errHandle = file('/home/admin/Logs/LogsSG001History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/SGDigest/SG001Digest/FTPSG001dump.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
LTCUTOFF = .001
FIREERRATA = 1
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins)/1)+1))
}
FIRETWILIONA=0
checkErrata = function(row,ts)
{
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA == 0)
	{
	  print(paste('Errata mail already sent','so no more mails'))
		return()
	}
	{
	if(!is.finite(as.numeric(row[2])))
	{
		FIRETWILIONA <<- FIRETWILIONA + 1
	}
	else
		FIRETWILIONA <<- 0
	}
	if((FIRETWILIONA > 5) || (as.numeric(row[2]) < LTCUTOFF && is.finite(as.numeric(row[2]))))
	{
		FIREERRATA <<- 0
		subj = paste('SG-001X','down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'\n\nTimestamp:',as.character(row[1]))
		body = paste(body,'\n\nReal Power Tot kW reading:',as.character(row[2]))
		if(FIRETWILIONA>5)
		{
		body = paste(body,'\n\nMore than 5 continuous readings are NA')
		}
		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("SG-001X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent '))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
										"\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "SG-001X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("SG-001X","TwilioAlert",as.character(row[1]))
		FIRETWILIONA <<-0
	}
	
}
stitchFile = function(path,days,pathwrite,erratacheck)
{
  x = 1
	t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
	{
		print(paste("Skipping",days[x],"Due to err in file"))
		return()
	}
  dataread = t
	if(nrow(dataread) < 1)
		return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
	{
	if(grepl('Gsi00',days[x]))
	idxvals = length(colnames)-1
	else if(grepl('Tmod',days[x]))
	idxvals = length(colnames)
	else
  idxvals = match(currcolnames,colnames)
  }
	for(y in 1 : nrow(dataread))
  {
	  ta = substr(as.character(dataread[y,1]),1,10)
		ta = format(as.Date(ta,'%m-%d-%Y'),'%Y-%m-%d')
		sp = unlist(strsplit(as.character(dataread[y,1])," "))
		dataread[y,1] = paste(ta,sp[2])
		idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritemon,filename,sep="/")
		{
		if(grepl("Gsi00",days[x]) || grepl('Tmod',days[x]))
		{
		  rowtemp2[idxvals] = as.numeric(dataread[y,2])
	 		rowtemp2[1] = as.character(dataread[y,1])
		}
		else
    rowtemp2[idxvals] = dataread[y,]
		}
		rowtemp2 = unlist(rowtemp2)
  {
    if(!file.exists(pathtowrite))
    {
			rowtemp2 =rbind(rowtemp2)
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rbind(unlist(rowtemp))
        }
				FIREERRATA <<- 1
				FIRETWILIONA <<-0
    }
    else
    {
      	df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
			{
				if(idxts > nrow(df))
				{
					df[idxts,]=rowtemp2
				}
				else
				{
					if(as.character(rowtemp2[1])==as.character(df[idxts,1]))
					{
						if(grepl("Gsi00",days[x]) || grepl("Tmod",days[x]))
						{
							 df[idxts,idxvals]=dataread[y,2]#dont replace with rowtemp2 this is correct
						}
						else
						df[idxts,(1:(length(rowtemp2)-2))] = rowtemp2[1:(length(rowtemp2)-2)]
					}
					else
					{
						print(paste('Mismatch Old time',as.character(df[idxts,1]),
						'New time',as.character(rowtemp2[1])))
						df[(idxts+1):(nrow(df)+1),] = df[idxts:nrow(df),]
						df[idxts,]=rowtemp2
					}
				}
      	#df[idxts,] = rowtemp2
			}
    }
  }
	if(erratacheck != 0 && (!grepl('Gsi00',days[x])) && (!grepl('Tmod',days[x])))
	{
	  pass = c(as.character(df[idxts,1]),as.character(as.numeric(df[idxts,15])/1000))
	  checkErrata(pass,idxts)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
	print(paste('Modified',pathtowrite))
}
	recordTimeMaster("SG-001X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX SG001 FTP Dump'
checkdir(path)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[SG-001X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'SG001.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
		idxtostart = c(1,1,1)
{
	if(!file.exists(pathdatelastread))
	{
	  print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[SG-001X]"')
		idxtostart = c(1,1,1)
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		#lastrecordeddate = as.character(lastrecordeddate[1])
		days2 = days[grepl('Gsi00',days)]
		days3 = days[grepl('Tmod',days)]
		days1 = days[grepl('PowerMeter1_',days)]

		idxtostart[1] = match(lastrecordeddate[1],days1)
		idxtostart[2] = match(lastrecordeddate[2],days2)
		idxtostart[3] = match(lastrecordeddate[3],days3)
		if(!is.finite(idxtostart[3]))
		idxtostart[3] = 1
		print(paste('Date read is',lastrecordeddate,'and idxtostart is',idxtostart))
	}
}
if(length(idxtostart) < 3)
idxtostart = c(idxtostart,1,1)

checkdir(pathwrite)
startidxnw = which(grepl("PowerMeter1_",days) %in% TRUE)
colnames = colnames(read.csv(paste(path,days[startidxnw[length(startidxnw)]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
colnames = c(colnames,"Gsi00","Tmod")
rowtemp = rep(NA,(length(colnames)))
x=1
stnname =  "[SG-001X"
{
	daysac = days
	days = daysac[grepl("PowerMeter1_",daysac)]
	if(idxtostart[1] < length(days))
	{
		print(paste('idxtostart is',idxtostart[1],'while length of days is',length(days)))
		for(x in idxtostart[1] : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days[x])
	}
	days = daysac[grepl("Gsi00",daysac)]
	if(is.finite(idxtostart[2]))
	{
	if(idxtostart[2] < length(days))
	{
		print(paste('idxtostart is',idxtostart[2],'while length of days is',length(days)))
		for(x in idxtostart[2] : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days[x])
	}
	}

	days = daysac[grepl("Tmod",daysac)]
	if(is.finite(idxtostart[3]))
	{
	if(idxtostart[3] < length(days))
	{
		print(paste('idxtostart is',idxtostart[3],'while length of days is',length(days)))
		for(x in idxtostart[3] : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[3] = as.character(days[x])
	}
}
	else
	{
		print('No historical backlog')
	}
}
days = daysac
print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
write(lastrecordeddate,file =pathdatelastread)

while(1)
{
	print('Checking FTP for new data')
 	filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
  daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
		if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	match = match[complete.cases(match)]
	print(length(match))
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
		print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,1)
		print(paste('Done',daysnew[x]))
	}
	daysnew2 = daysnew[grepl('Gsi00',daysnew)]
	daysnew3 = daysnew[grepl('Tmod',daysnew)]
	daysnew = daysnew[grepl('PowerMeter1_',daysnew)]
	if(length(daysnew))
	lastrecordeddate[1] = as.character(daysnew[length(daysnew)])
	if(length(daysnew2))
	lastrecordeddate[2] =as.character(daysnew2[length(daysnew2)])
	if(length(daysnew3))
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])

	write(c(lastrecordeddate[1],
	lastrecordeddate[2],
	lastrecordeddate[3]),pathdatelastread)
	gc()
}
print('Exit Loop')
sink()
