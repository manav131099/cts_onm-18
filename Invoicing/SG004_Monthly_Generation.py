import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import math
import sys
import numpy as np
import pyodbc
import logging
import smtplib
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

def send_mail(info,attachment_path_list=None):
    s = smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    recipients = ['sai.pranav@cleantechsolar.com','jesse.christopher@cleantechsolar.com','adam.syed@cleantechsolar.com']
    msg['Subject'] = "SG-004 Energy Report"
    if sender is not None:
        msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            try:
                file_name=each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
            except:
                print ("could not attache file")
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'
path='/home/admin/Dropbox/Second Gen/'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

eac = pd.read_sql_query('''  SELECT  [Date],[Eac-MFM] FROM [dbo].[Stations_Data]  WHERE [Station_Id]=87 ''', connStr)
df_eac = pd.DataFrame(eac, columns=['Date','Eac-MFM'])

connStr.close()

print(df_eac)

df_eac['Date']=pd.to_datetime(df_eac['Date'])

df_eac.index=df_eac['Date']

df_monthly=df_eac[['Eac-MFM']].resample("1m").sum()

df_monthly['CO2 Avoided (kg)']=df_monthly['Eac-MFM']*0.4188



df_monthly=df_monthly.round(0)

df_monthly.index=df_monthly.index.strftime('%b-%y')
df_monthly.index.name = 'Date'
print(df_monthly)
df_monthly.columns=['Energy Generated (kWh)','CO2 Avoided (kg)']
df_monthly.to_csv('/home/pranav/Monthly Energy Generation Report.txt',sep='\t')

send_mail("Please find the report attached.",['/home/pranav/Monthly Energy Generation Report.txt'])